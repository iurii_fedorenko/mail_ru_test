package steps;

import bo.Account;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import ui.pages.SignUpPage;

public class SignUpPageSteps {
//    private static WebDriver driver = Driver.getWebDriverInstance();
    private Account account = new Account();
//    private SignUpPage signUpPage = new SignUpPage();

    @Given("^User is on the 'SingUp' page$")
    public void navigate_to_SignUp_page() {
        new SignUpPage().openSignUpPage();

    }

    @When("^User enters \"([^\"]*)\" on the 'First Name' textbox$")
    public void iEnterOnFirstNameTextbox(String firstName){
        account.setFirstName(firstName);
        new SignUpPage().enterFirstName(firstName);
    }

    @When("^User enters \"([^\"]*)\" on the 'Last Name' textbox$")
    public void userEntersOnLastNameTextbox(String lastName){
        account.setLastName(lastName);
        new SignUpPage().enterLastName(lastName);
    }

    @When("^User selects \"([^\"]*)\" on the 'Day' dropdown$")
    public void userSelectOnTheDayDropdown(String day){
        account.setBirthdayDay(day);
        new SignUpPage().selectDay(day);
    }

    @When("^User selects \"([^\"]*)\" on the 'Month' dropdown$")
    public void userSelectOnTheMonthDropdown(String month){
        account.setBirthdayMonth(month);
        new SignUpPage().selectMonth(month);
    }

    @When("^User selects \"([^\"]*)\" on the 'Year' dropdown$")
    public void userSelectsOnTheYearDropdown(String year){
        account.setBirthdayYear(year);
        new SignUpPage().selectYear(year);
    }

    @When("^User selects \"([^\"]*)\" on the 'Sex' radiobutton$")
    public void userSelectsOnTheSexRadiobutton(String sex){
        account.setSex(sex);
        new SignUpPage().selectSex(sex);
    }

    @When("^User enters generated email on the 'Email' textbox$")
    public void userEntersGeneratedEmailOnTheEmailTextbox(){
        String email = account.getFirstName() + "_" + account.getLastName() + "_" + (int)(Math.random() * 100000);
        account.setEmail(email);
        new SignUpPage().enterEmail(email);
    }

    @When("^User enters \"([^\"]*)\" on the 'Password' textbox$")
    public void userEntersOnThePasswordTextbox(String password){
        account.setPassword(password);
        new SignUpPage().enterPassword(password);
    }

    @When("^User retries password on the 'Retry Password' textbox$")
    public void userRetriesPasswordOnTheRetryPasswordTextbox(){
        new SignUpPage().retryPassword(account.getPassword());
    }

    @When("^User clicks on the 'SignUp' button$")
    public void userClicksOnTheSignUpButton(){
        new SignUpPage().clickSignUpButton();
    }

    @When("^User enters \"([^\"]*)\" on the 'Additional Email' testbox$")
    public void userEntersOnTheAdditionalEmailTestbox(String additionalEmail){
        new SignUpPage().enterAdditionalEmail(additionalEmail);
    }
}
