package steps;

import bo.Account;
import ui.pages.SignUpPage;
import ui.pages.SignUpVerifyPage;

public class SignUpPageStepsTestNG {
    private SignUpPageStepsTestNG(){}
    public static void fillNewAccountForm(Account account){
        SignUpPage signUpPage = new SignUpPage();
        signUpPage.openSignUpPage().enterFirstName(account.getFirstName()).enterLastName(account.getLastName())
                .selectDay(account.getBirthdayDay()).selectMonth(account.getBirthdayMonth()).selectYear(account.getBirthdayYear())
                .selectSex(account.getSex()).enterEmail(account.getEmail())
                .enterPassword(account.getPassword()).retryPassword(account.getPassword()).clickSignUpButton();
    }

    public static boolean isFormFilled(){
        return (new SignUpVerifyPage()).isSignUpVerifyPageOpened();
    }
}
