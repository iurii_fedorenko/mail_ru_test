package steps;

import cucumber.api.java.en.Then;
import org.testng.Assert;
import ui.pages.SignUpVerifyPage;

public class SignUpVerifyPageSteps {

    @Then("'SignUpVerify' page is opened$")
    public void SignUpVerifyPageIsOpened() {
        Assert.assertTrue(new SignUpVerifyPage().isSignUpVerifyPageOpened(), "SignUp form is not filled or filled incorrectly");
    }
}
