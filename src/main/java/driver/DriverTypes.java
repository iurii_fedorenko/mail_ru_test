package driver;

public enum DriverTypes {
    FIREFOX, IE, CHROME, GRID, CLOUD;
}
