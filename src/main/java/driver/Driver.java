package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.ConfigClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Driver {
    private static final String DEFAULT_DRIVER_TYPE = "CHROME";
    private static WebDriver instance;

    private Driver(){}

    public static WebDriver getWebDriverInstance(String driverType){
        if (!isInstanceExist()) {
            DriverTypes type;
            try {
                type = DriverTypes.valueOf(driverType);
            } catch (IllegalArgumentException|NullPointerException e){
                type = DriverTypes.valueOf(DEFAULT_DRIVER_TYPE);
            }
            try {
                switch (type) {
                    case FIREFOX: {
                        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                        instance = new FirefoxDriver();
                        break;
                    }
                    case IE: {
                        System.setProperty("webdriver.ie.driver", "src/main/resources/IEDriverServer.exe");
                        instance = new InternetExplorerDriver();
                        break;
                    }
                    case GRID: {
                        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                        instance = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
                        break;
                    }
                    case CLOUD: {
                        DesiredCapabilities caps = DesiredCapabilities.chrome();
                        caps.setCapability("platform", "Windows 7");
                        caps.setCapability("version", "53.0");
                        instance = new RemoteWebDriver(new URL("http://yfedorenko:6205c417-1d7d-45eb-981d-d003f80bcc53@ondemand.saucelabs.com:80"), caps);
                        break;
                    }
                    default:
                        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                        instance = new ChromeDriver();
                }
                instance.manage().timeouts().pageLoadTimeout(ConfigClass.getTimeout(), TimeUnit.SECONDS);
                instance.manage().timeouts().setScriptTimeout(ConfigClass.getTimeout(), TimeUnit.SECONDS);
                instance.manage().timeouts().implicitlyWait(ConfigClass.getTimeout(), TimeUnit.SECONDS);
                instance.manage().deleteAllCookies();
                instance.manage().window().maximize();
            } catch (RuntimeException|MalformedURLException e){
                e.printStackTrace();
            }
        } else {
            return instance;
        }
        return instance;
    }

    public static WebDriver getWebDriverInstance(){
        return getWebDriverInstance(DEFAULT_DRIVER_TYPE);
    }

    public static boolean isInstanceExist(){
        if (instance != null){
            return true;
        } else
            return false;
    }


    public static void killWebDriverInstance() {
        if (instance != null) {
            try {
                instance.quit();
                instance = null;
            } catch (WebDriverException ex) {
            }
        }
    }
}
