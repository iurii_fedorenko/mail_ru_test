package ui.pages;

import driver.Driver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AppLogger;
import utils.Screenshoter;

public class AbstractPage {
    private static final int WAIT_FOR_ELEMENT_TIMEOUT_SECONDS = 30;

    protected void openPage(String pageURL){
        if (!isPageOpen(pageURL)) {
            Driver.getWebDriverInstance().get(pageURL);
            waitPageLoading(pageURL);
        }
        if (isPageOpen(pageURL)){
            AppLogger.debug("Page " + pageURL + " open");
        } else {
            AppLogger.info("Page " + pageURL + " doesn't open");
            Screenshoter.takeScreenshot();
        }
    }

    protected boolean isPageOpen (String pageURL){
        String currentPage = Driver.getWebDriverInstance().getCurrentUrl();
        return currentPage.equals(pageURL);
//        return Driver.getWebDriverInstance().getCurrentUrl().equals(pageURL);
    }

    protected void waitPageLoading (String pageURL){
        new WebDriverWait(Driver.getWebDriverInstance(), WAIT_FOR_ELEMENT_TIMEOUT_SECONDS).until(ExpectedConditions.urlContains(pageURL));
    }
}
