package ui.pages;

import driver.Driver;
import org.openqa.selenium.By;
import ui.elements.GenericElement;
import ui.elements.RadioButton;
import ui.elements.TextBox;

public class SignUpPage extends AbstractPage {
    private static final String SIGNUP_PAGE_URL = "https://account.mail.ru/signup/simple";

    private static final By FIRST_NAME_TEXTBOX = By.xpath("//input[@name=\"firstname\"]");
    private static final By LAST_NAME_TEXTBOX = By.xpath("//input[@name=\"lastname\"]");
    private static final By SEX_RADIOBUTTON = By.xpath("//div[@class=\"b-radiogroup__radio-border\"]");
    private static final By EMAIL_TEXTBOX = By.xpath("//div[@required=\"true\"]//input[@data-blockid=\"email_name\"]");
    private static final By EMAIL_DOMAIN_DROPDOWN = By.xpath("//span[@class=\"b-email__domain\"]//select");
    private static final By PASSOWORD_TEXTBOX = By.xpath("//input[@name=\"password\"]");
    private static final By PASSOWORD_RETRY_TEXTBOX = By.xpath("//input[@name=\"password_retry\"]");
    private static final By ADDITIONAL_EMAIL_TEXTBOX = By.xpath("//span[@class=\"b-email__name\"]//input[@name=\"additional_email\"]");
    private static final By COUNTRIES_DROPDOWN = By.xpath("//select[@name=\"SelectPhoneCode\"]");
    private static final By PHONE_TEXTBOX = By.xpath("//input[@class=\"phone\"]");
    private static final By SIGNUP_BUTTON = By.xpath("//button[@data-blockid=\"btn\"]/span");
    private static final By PHONE_INFO_TEXT = By.id("phoneInfoContainer");
    private static final By NO_PHONE_LINK = By.id("noPhoneLink");
    private static final By EMAIL_UNIQUENESS_FLAG = By.xpath("//div[@id=\"loginField\"]//span[@class=\"success\"]");

    private static final By BIRTHDAY_DAY_DIV = By.xpath("//div[@class=\"b-date__day\"]");
    private static final By BIRTHDAY_DAY_DROPDOWN = By.xpath("//div[@class=\"b-date__day\"]//a");
    private static final By BIRTHDAY_MONTH_DIV = By.xpath("//div[@class=\"b-date__month\"]");
    private static final By BIRTHDAY_MONTH_DROPDOWN = By.xpath("//div[@class=\"b-date__month\"]//a");
    private static final By BIRTHDAY_YEAR_DIV = By.xpath("//div[@class=\"b-date__year\"]");
    private static final By BIRTHDAY_YEAR_DROPDOWN = By.xpath("//div[@class=\"b-date__year\"]//a");


    public SignUpPage openSignUpPage(){
        openPage(SIGNUP_PAGE_URL);
        return this;
    }

    public SignUpPage enterFirstName(String name){
        new TextBox(Driver.getWebDriverInstance(), FIRST_NAME_TEXTBOX).fillText(name);
        return this;
    }

    public SignUpPage enterLastName(String surname){
        new TextBox(Driver.getWebDriverInstance(), LAST_NAME_TEXTBOX).fillText(surname);
        return this;
    }

    public SignUpPage enterEmail(String emailLocal){
        new TextBox(Driver.getWebDriverInstance(), EMAIL_TEXTBOX).fillText(emailLocal);
        return this;
    }

    public SignUpPage enterPassword(String pass){
        new TextBox(Driver.getWebDriverInstance(), PASSOWORD_TEXTBOX).fillText(pass);
        return this;
    }

    public SignUpPage retryPassword(String pass){
        new TextBox(Driver.getWebDriverInstance(), PASSOWORD_RETRY_TEXTBOX).fillText(pass);
        return this;
    }

    public SignUpPage selectDay(String day){
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_DAY_DIV).clickElement();
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_DAY_DROPDOWN).findElementFromListAndClick("data-text", day);
        return this;
    }

    public SignUpPage selectMonth(String month){
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_MONTH_DIV).clickElement();
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_MONTH_DROPDOWN).findElementFromListAndClick("data-text", month);
        return this;
    }

    public SignUpPage selectYear(String year){
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_YEAR_DIV).clickElement();
        new GenericElement(Driver.getWebDriverInstance(), BIRTHDAY_YEAR_DROPDOWN).findElementFromListAndClick("data-text", year);
        return this;
    }


    public SignUpPage selectSex(String sex){
        new RadioButton(Driver.getWebDriverInstance(), SEX_RADIOBUTTON).switchRB(sex);
        return this;
    }

    public SignUpPage enterAdditionalEmail(String additionalEmail){
        new TextBox(Driver.getWebDriverInstance(), ADDITIONAL_EMAIL_TEXTBOX).fillText(additionalEmail);
        return this;
    }


    public SignUpVerifyPage clickSignUpButton(){
        new GenericElement(Driver.getWebDriverInstance(), SIGNUP_BUTTON).clickElementThrowJS();
        return new SignUpVerifyPage();
    }


    public boolean isSignUpPageLoaded(){
        return isPageOpen(SIGNUP_PAGE_URL);
    }
}
