package ui.pages;

import driver.Driver;
import org.openqa.selenium.By;
import ui.elements.WebHrefLink;

/**
 * Created by Iurii_Fedorenko on 7/19/2017.
 */
public class MainPage extends AbstractPage {
    private static final String MAIN_PAGE_URL = "https://mail.ru/";
    private static final By REGISTRATION_LINK_LOCATOR = By.xpath("//a[@class=\"mailbox__register__link\"]");

    public SignUpPage clickRegistrationLink (){
        new WebHrefLink(Driver.getWebDriverInstance(), REGISTRATION_LINK_LOCATOR).clickElement();
        return new SignUpPage();
    }

    public MainPage openMainPage(){
        openPage(MAIN_PAGE_URL);
        return this;
    }
}
