package ui.pages;

public class SignUpVerifyPage extends AbstractPage {
    private static final String SIGNUP_PAGE_URL = "https://account.mail.ru/signup/verify";

    public boolean isSignUpVerifyPageOpened(){
        waitPageLoading(SIGNUP_PAGE_URL);
        return isPageOpen(SIGNUP_PAGE_URL);
    }
}
