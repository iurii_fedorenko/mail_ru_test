package ui.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.AppLogger;
import utils.Screenshoter;

public class RadioButton extends GenericElement {

    public RadioButton(WebDriver driver, By by) {
        super(driver, by);
    }

    public void switchRB(String rbValue) {
        if (2==driver.findElements(elementLocator).size()) {
            if (rbValue.equals("male")) {
                highlightElement();
                driver.findElements(elementLocator).get(0).click();
                AppLogger.info("Rediobutton is switched to male");
                unhighlightElement();
            } else if (rbValue.equals("female")) {
                highlightElement();
                driver.findElements(elementLocator).get(1).click();
                AppLogger.info("Rediobutton is switched to female");
                unhighlightElement();
            } else {
                AppLogger.error("RadioButton doesn't contain " + rbValue + " value");
                Screenshoter.takeScreenshot();
            }
        }
    }
}
