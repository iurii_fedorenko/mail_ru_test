package ui.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.AppLogger;

public class TextBox extends GenericElement {


    public TextBox(WebDriver driver, By by) {
        super(driver, by);
    }

    public void clearText(){
        waitElementPresent();
        highlightElement();
        driver.findElement(elementLocator).clear();
        AppLogger.info("Textbox with locator: " + elementLocator.toString() + " is cleared");
        unhighlightElement();
    }

    public void fillText(String text){
        waitElementPresent();
        clearText();
        highlightElement();
        driver.findElement(elementLocator).sendKeys(text);
        AppLogger.info("Text \"" + text + "\" is entered to textbox with locator: " + elementLocator.toString());
        unhighlightElement();
    }
}
