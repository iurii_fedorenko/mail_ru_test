package ui.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import utils.AppLogger;

/**
 * Created by Iurii_Fedorenko on 7/19/2017.
 */
public class DropDown extends GenericElement{


    public DropDown(WebDriver driver, By by) {
        super(driver, by);
    }

    public void selectDropDownListItem(String listItem){
        waitElementPresent();
        new Select(driver.findElement(elementLocator)).selectByValue(listItem);
        AppLogger.info("Dropdown list item with locator: " + listItem + " is selected");
    }
}
