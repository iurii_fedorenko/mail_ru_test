package ui.elements;

import driver.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AppLogger;
import utils.Screenshoter;

public class GenericElement{
//    private WebElement element;
    private static final int WAIT_FOR_ELEMENT_TIMEOUT_SECONDS = 30;
    protected WebDriver driver;
    protected By elementLocator;


    public GenericElement(WebDriver driver, By by) {
        this.driver = Driver.getWebDriverInstance();
        elementLocator = by;
    }

    public boolean isElementPresentAndSingle() {
        int elementQtty = Driver.getWebDriverInstance().findElements(elementLocator).size();
        switch (elementQtty){
            case 1:
                return true;
            case 0: {
                AppLogger.error("Element with locator: " + elementLocator.toString() + " not found");
                Screenshoter.takeScreenshot();
                return false;
                }
            default: {
                AppLogger.error("More than one elements were found by locator: " + elementLocator.toString());
                Screenshoter.takeScreenshot();
                return false;
            }
        }
    }

    public boolean isElementEnabled(){
        boolean elementEnabled = isElementPresentAndSingle()&&driver.findElement(elementLocator).isEnabled();
        if (elementEnabled == false){
            AppLogger.error("Element with locator: " + elementLocator.toString() + " is not enabled");
            Screenshoter.takeScreenshot();
        }
        return elementEnabled;
    }

    protected void waitElementPresent(){
        if (!isElementEnabled()){
            new WebDriverWait(driver, WAIT_FOR_ELEMENT_TIMEOUT_SECONDS).until(ExpectedConditions.elementToBeSelected(elementLocator));
        }
    }

    public void clickElement(){
        waitElementPresent();
        highlightElement();
        driver.findElement(elementLocator).click();
        AppLogger.info("Element with locator: " + elementLocator.toString() + " is clicked");
        unhighlightElement();
    }

    public void clickElementThrowJS(){
        waitElementPresent();
        WebElement element = driver.findElement(elementLocator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].style.border='5px solid green'", element);
        executor.executeScript("arguments[0].click();", element);
        AppLogger.info("Element with locator: " + elementLocator.toString() + " is clicked");
        executor.executeScript("arguments[0].style.border='0px'", element);
    }

    public void findElementFromListAndClick(String attributeName, String attributeValue){
        if(driver.findElements(elementLocator).size()!= 0){
            for(WebElement element: driver.findElements(elementLocator)){
                if(element.getAttribute(attributeName).equals(attributeValue))
                    element.click();
            }
        } else {
            AppLogger.info("Elements list with locator: " + elementLocator.toString() + " is empty");
            Screenshoter.takeScreenshot();
        }
    }

    public void highlightElement(){
        WebElement element = driver.findElement(elementLocator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].style.border='5px solid green'", element);
    }

    public void unhighlightElement(){
        WebElement element = driver.findElement(elementLocator);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].style.border='0px'", element);
    }



}
