package utils;

public class ConfigClass {
    private static int waitElementTimeout = 30;
    private static int timeout = 30;

    public static int getWaitElementTimeout() {
        return waitElementTimeout;
    }


    public static int getTimeout() {
        return timeout;
    }

    private  ConfigClass() {
    }


}
