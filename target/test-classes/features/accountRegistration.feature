@simple_test
Feature: New account registration

  Scenario: Fill new account registration form
    Given User is on the 'SingUp' page
    When User enters "Ivan" on the 'First Name' textbox
    When User enters "Ivanov" on the 'Last Name' textbox
    When User selects "1" on the 'Day' dropdown
    When User selects "Март" on the 'Month' dropdown
    When User selects "1977" on the 'Year' dropdown
    When User selects "male" on the 'Sex' radiobutton
    When User enters generated email on the 'Email' textbox
    When User enters "AA12345" on the 'Password' textbox
    When User retries password on the 'Retry Password' textbox
    When User enters "test@test.com" on the 'Additional Email' testbox
    When User clicks on the 'SignUp' button
    Then 'SignUpVerify' page is opened



