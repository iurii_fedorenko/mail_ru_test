$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/accountRegistration.feature");
formatter.feature({
  "line": 2,
  "name": "New account registration",
  "description": "",
  "id": "new-account-registration",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@simple_test"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Fill new account registration form",
  "description": "",
  "id": "new-account-registration;fill-new-account-registration-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "User is on the \u0027SingUp\u0027 page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User enters \"Ivan\" on the \u0027First Name\u0027 textbox",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "User enters \"Ivanov\" on the \u0027Last Name\u0027 textbox",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "User selects \"1\" on the \u0027Day\u0027 dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "User selects \"Март\" on the \u0027Month\u0027 dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User selects \"1977\" on the \u0027Year\u0027 dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "User selects \"male\" on the \u0027Sex\u0027 radiobutton",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User enters generated email on the \u0027Email\u0027 textbox",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "User enters \"AA12345\" on the \u0027Password\u0027 textbox",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "User retries password on the \u0027Retry Password\u0027 textbox",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "User enters \"test@test.com\" on the \u0027Additional Email\u0027 testbox",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User clicks on the \u0027SignUp\u0027 button",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "\u0027SignUpVerify\u0027 page is opened",
  "keyword": "Then "
});
formatter.match({
  "location": "SignUpPageSteps.navigate_to_SignUp_page()"
});
formatter.result({
  "duration": 8033869657,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ivan",
      "offset": 13
    }
  ],
  "location": "SignUpPageSteps.iEnterOnFirstNameTextbox(String)"
});
formatter.result({
  "duration": 1834371678,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ivanov",
      "offset": 13
    }
  ],
  "location": "SignUpPageSteps.userEntersOnLastNameTextbox(String)"
});
formatter.result({
  "duration": 1324883682,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 14
    }
  ],
  "location": "SignUpPageSteps.userSelectOnTheDayDropdown(String)"
});
formatter.result({
  "duration": 2371627946,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Март",
      "offset": 14
    }
  ],
  "location": "SignUpPageSteps.userSelectOnTheMonthDropdown(String)"
});
formatter.result({
  "duration": 1129092639,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1977",
      "offset": 14
    }
  ],
  "location": "SignUpPageSteps.userSelectsOnTheYearDropdown(String)"
});
formatter.result({
  "duration": 4490276743,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "male",
      "offset": 14
    }
  ],
  "location": "SignUpPageSteps.userSelectsOnTheSexRadiobutton(String)"
});
formatter.result({
  "duration": 391702084,
  "status": "passed"
});
formatter.match({
  "location": "SignUpPageSteps.userEntersGeneratedEmailOnTheEmailTextbox()"
});
formatter.result({
  "duration": 2050062782,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "AA12345",
      "offset": 13
    }
  ],
  "location": "SignUpPageSteps.userEntersOnThePasswordTextbox(String)"
});
formatter.result({
  "duration": 1572687487,
  "status": "passed"
});
formatter.match({
  "location": "SignUpPageSteps.userRetriesPasswordOnTheRetryPasswordTextbox()"
});
formatter.result({
  "duration": 942644529,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "test@test.com",
      "offset": 13
    }
  ],
  "location": "SignUpPageSteps.userEntersOnTheAdditionalEmailTestbox(String)"
});
formatter.result({
  "duration": 2140764890,
  "status": "passed"
});
formatter.match({
  "location": "SignUpPageSteps.userClicksOnTheSignUpButton()"
});
formatter.result({
  "duration": 713526981,
  "status": "passed"
});
formatter.match({
  "location": "SignUpVerifyPageSteps.SignUpVerifyPageIsOpened()"
});
formatter.result({
  "duration": 592744653,
  "error_message": "java.lang.AssertionError: SignUp form is not filled or filled incorrectly expected [true] but found [false]\r\n\tat org.testng.Assert.fail(Assert.java:94)\r\n\tat org.testng.Assert.failNotEquals(Assert.java:513)\r\n\tat org.testng.Assert.assertTrue(Assert.java:42)\r\n\tat steps.SignUpVerifyPageSteps.SignUpVerifyPageIsOpened(SignUpVerifyPageSteps.java:11)\r\n\tat ✽.Then \u0027SignUpVerify\u0027 page is opened(src/test/resources/features/accountRegistration.feature:17)\r\n",
  "status": "failed"
});
});