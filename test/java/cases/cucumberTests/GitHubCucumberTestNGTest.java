package cases.cucumberTests;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import driver.Driver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

@CucumberOptions(
            strict = true,
            monochrome = true,
            glue = {"steps"},
            features="src/test/resources/features/accountRegistration.feature",
//            plugin = { "pretty", "json:target/cucumber-report.json",
//                    "html:target/cucumber-report"
//            }
        plugin = {"pretty", "com.epam.reportportal.cucumber.ScenarioReporter"}
    )
public class GitHubCucumberTestNGTest extends AbstractTestNGCucumberTests {

        @BeforeClass
        public void setUp() {
            Driver.getWebDriverInstance(System.getProperty("browser"));
        }

        @AfterClass
        public void closeBrowser() throws Exception {
            Driver.killWebDriverInstance();
        }


    }

