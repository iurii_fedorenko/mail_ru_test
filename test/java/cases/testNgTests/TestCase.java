package cases.testNgTests;

import bo.Account;
import driver.Driver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import steps.SignUpPageStepsTestNG;

public class TestCase {
    WebDriver driver;

    @BeforeMethod(description = "Webdriver initialization", enabled = false)
    public void setUp(){
        driver = Driver.getWebDriverInstance(System.getProperty("browser"));
    }

    @Test(description = "Verify email sending", enabled = false)
    public void openPage() {
        Account newAccount = new Account();
        newAccount.setFirstName("Ivan");
        newAccount.setLastName("Ivanov");
        newAccount.setBirthdayDay("10");
        newAccount.setBirthdayMonth("Март");
        newAccount.setBirthdayYear("1977");
        newAccount.setSex("male");
        newAccount.setEmail("ivanov_ivan_1977" + (int)(Math.random() * 100000));
        newAccount.setEmailDomain("@mail.ru");
        newAccount.setPassword("AA12345");
        newAccount.setCountry("США");
        newAccount.setPhoneNumber("6106106161");

        SignUpPageStepsTestNG.fillNewAccountForm(newAccount);

        Assert.assertTrue(SignUpPageStepsTestNG.isFormFilled());
    }

    @AfterMethod(enabled = false)
    public void closeBrowser(){
        Driver.killWebDriverInstance();
    }
}
