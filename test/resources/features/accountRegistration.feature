@simple_test
Feature: New account registration

  Scenario: Fill new account registration form
    Given User is on the 'SingUp' page
    When User enters "Ivan" on the 'First Name' textbox
    And User enters "Ivanov" on the 'Last Name' textbox
    And User selects "1" on the 'Day' dropdown
    And User selects "March" on the 'Month' dropdown
    And User selects "1977" on the 'Year' dropdown
    And User selects "male" on the 'Sex' radiobutton
    And User enters generated email on the 'Email' textbox
    And User enters "AA12345" on the 'Password' textbox
    And User retries password on the 'Retry Password' textbox
    And User enters "test@test.com" on the 'Additional Email' testbox
    And User clicks on the 'SignUp' button
    Then 'SignUpVerify' page is opened



